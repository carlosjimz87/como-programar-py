
'''
LISTAS

 * MAYOR VELOCIDAD EN:
 * - aañadir elemento
 * - eliminar elimento
 * - modificar elementos
 * 
 * PERMITE 
 * - ordenar elementos
 
'''
print("***********************LISTAS*******************************")

# Como crear una lista
lista = [None, 2, "Hola", 4.56, False]  # array - arreglo - lista
lista2 = list((None, 2, "hola", 4.56, False))
# imprimiendo la lista
print(lista)
# imprimiendo el tipo de dato de la lista
print(type(lista))
# extrayendo un elemento de la lista
print(lista[2])

# iterando los elementos de la lista para imprimirlos individualmente
for v in lista:
    print(v, end=',')
print("")

# modificando la lista
lista[2] = "Mundo"
lista[4] = True
print(lista)

#  anulando un elemnto de la lista
lista[4] = None
print(lista)

# añadir un elemento de la lista al final
lista.append("Santi")
print(lista)

# eliminar un elemento de la lista (por valor)
lista.remove("Santi")
print(lista)

# eliminar un elemento de la lista (por indice)
lista.pop(0)
print(lista)

# error de indexado al acceder a la lista --> ERROR (list index out of range)
# print(lista[200])

print(lista[2])

print("***********************TUPLAS*******************************")
# Como crear una tuplas
tupla = (None, 2, "Hola", 4.56, False)  # array - arreglo - lista
tupla2 = tuple((None, 2, "hola", 4.56, False))

print(tupla)
print(type(tupla))
print(tupla2)
print(type(tupla2))

print("---------------------------------")

listadetupla = list(tupla)
listadetupla.append(1)
tupla = tuple(listadetupla)
print(tupla)
