# # CICLOS DEFINIDOS

# un ciclo que imprima 10 veces hola con las iteraciones de uno en uno

# for i in range(10):
#     print("hola", i)

# un ciclo que imprima 5 veces hola con las iteraciones de dos en dos

# for i in range(0, 10, 2):
#     print("hola", i)

i = 0
while i < 10:
    print("hola", i)
    i += 1


