# #  Ejercicio 1
# # Cree una función que permita recibir una palabra y devuelva el número de letras que esta contiene.
# def word(entrada):
#     return len(entrada)


# output = word("santi")
# print(output)


# #  Ejercicio 2
# # Cree una función que imprima los números pares entre 0 y 100.
# def num():
#     for i in range(0, 100, 2):
#         print(i)


# num()
#  Ejercicio 3
# Cree una función que almacene los días de la semana (Lunes, Martes, Miércoles,
# Jueves, Viernes) y que permita eliminar un día de la semana cualquiera y devuelva
# los nombres restantes. Para esto, debe permitir que la función reciba el nombre
# del día de la semana a eliminar, y devolver los restantes nombres en una lista.


def days(input):
    dias = ["lunes", "martes", "miercoles",
            "jueves", "viernes", "sabado", "domingo"]
    dias.remove(input)
    return dias


diasRestantes = days("viernes")
print(diasRestantes)


# Ejercicio 4
# Con la lista que devuelve la función del Ejercicio 3, realice otra función que
# imprima pares de datos de la siguiente forma:

# {0:'Viernes', 1:'Lunes', 2:'Sabado', 3:'Jueves', 4:'Martes', 5:'Miércoles'}

# (asumimos que la lista de días de la semana tendrá todos menos el Domingo,
# NO TENGA EN CUENTA EL ORDEN, SOLAMENTE EL FORMATO DEL TEXTO DE EJEMPLO)

def convertirDicc(lista):
    dic = {}  # dict()
    for i in range(len(lista)):
        dic[i] = lista[i]

    print(dic)


def convertirSet(lista):
    _set = set()  # dict()
    for v in lista:
        _set.add(v)

    print(_set)


convertirDicc(diasRestantes)
convertirSet(diasRestantes)
