
""" 
# LISTAS

# creando listas
lista1 = [1, 2, 3, 4, 5, 1]
lista2 = list()

print("Lista1:", lista1)
print("Lista2:", lista2)

# adicionando elementos
lista1.append(1)
lista1.append(2)
lista1.append(3)
lista1.append(4)
print("Lista1:", lista1)

print("-------------")
# accediendo elementos
a = lista1[0]
print(a)

print("-------------")
# # extrayendo elementos
elem = lista1.pop()
print("Lista1:", lista1)
print("Elem:", elem)

print("-------------")

# # eliminando elementos
lista1.remove(3)
print("Lista1:", lista1)

print("-------------")

# # iterando elementos
lista3 = ["a", "b", "c", "d"]

for i in range(len(lista3)):    # recorrer lista        for (var i =0; i<lista3.length; i++)
    print(str(i+1)+"-"+lista3[i])

print("-------------")

for i in lista3:    # iterar lista
    print(i)


# TUPLAS

# creando tuplas
tuple1 = (1, 2, 3, 4, 5, 1)
tuple2 = tuple((1, 2, 3, 4))

print(tuple1)
print(tuple2)

# no permite cambios ( es inmutable)

# tuple1[0] = 2     ERROR: No permite mutabilidad
# tuple2[0] = 2     ERROR: No permite mutabilidad


# DICTIONARIES

# creando diccionarios
dicc1 = {1: "a", 2: "b", 3: "c", 4: "d"}
dicc2 = dict()

print(dicc1)
print(dicc2)

# añadiendo elementos

dicc1[5] = "e"
print(dicc1)

print("-------------")

# extrayendo elementos

elem = dicc1.pop(1)
print(dicc1)
print("elem:", elem)

# eliminando elementos
print("-------------")

del dicc1[4]
print(dicc1)

# iterando elementos

print("-------------")
# por items
for k, v in dicc1.items():
    print(k, v)

print("-------------")
# por values
for v in dicc1.values():
    print(v)
print("-------------")

# por llaves
for k in dicc1.keys():
    print(k)

# no permite llaves duplicadas

diccA = {1: "a", 2: "b", 3: "c", 4: "d", 4: "e"}
print(diccA)    # toma solo el ultimo valor de la llave duplicada

 """

# SET

set1 = {2, 5, 8, 6}
set2 = set()

print(set1)
print(set2)

print("-------------")
# añadiendo elementos

set1.add(7)
print(set1)

print("-------------")

# comprobar si un elemento está en el Set

if 5 in set1:
    print("esta")
else:
    print("no esta")

print("-------------")


# no permite valores duplicados

set3 = {2, True, "Hola", 1.4, 2}
print(set3)    # toma solo el ultimo valor de la llave duplicada

print("-------------")
# iterando elementos

for k in set3:
    print(k)
