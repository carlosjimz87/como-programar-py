a = 1
b = 0

########## CONDICIONALES #################

# operador AND

# a   b   RESULTADO ( 0 = false, 1 = true)
# 0   0   0
# 0   1   0
# 1   0   0
# 1   1   1

### 1 .if else ###

# if a and b:  # = 1
#     print("Todos son unos")

# else:   # = 0
#     print("Al menos hay un cero")


### 2 .if else anidado ###


# if a and b:  # = 1
#     print("Todos son unos")


# else:   # = 0
#     print("Al menos hay un cero")

#     if a == 0 and b == 0:  # ambas variables son 0
#         print("Ambos son cero")


### 3 .if elseif else ###


# if a and b:  # TODOS UNO
#     print("Todos son unos")

# elif a == 0 and b == 0:  # TODOS CERO
#     print("Ambos son cero")

# else:   # AL MENOS UN CERO
#     print("Al menos hay un cero")
