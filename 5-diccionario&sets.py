'''
DICCIONARIOS
 
'''

# crear un diccionario
midic = {"a": 3,
         1: "dos",
         "c": True,
            False: "boleano"}

midic2 = dict()
midic2["1"] = 1
midic2["2"] = 2
midic2["3"] = 3
midic2["4"] = 4

# print(midic)
# print(type(midic))
# print(midic2)
# print(type(midic2))
# print("------------------------------------")

# # extrayendo un elemento del diccionario
# print(midic["a"])
# print("------------------------------------")

# # iterar solo por valor
# for val in midic.values():
#     print(val)
# print("------------------------------------")

# # iterar solo por llave
# for key in midic.keys():
#     print(key)

# print("------------------------------------")
# # iterar por llave y valor
# for key, value in midic.items():
#     print(key, value)


# '''
# SETS

# '''

# # crear un set
miset = {3, "dos", True, "boleano"}
miset2 = set((2, "dos", False, "otro"))


print(miset)
print(type(miset))
print(miset2)
print(type(miset2))


print("------------------------------------")

# extraer elementos
val = miset.pop()
print(val)

# print("------------------------------------")

# # añadir elementos
# miset.add(5)
# print(miset)

# print("------------------------------------")

# # iterar por valor
# for val in miset:
#     print(val)

# print("------------------------------------")

# # comprobar existencias
# val = True
# if val in miset:
#     print("ESTA")
# else:
#     print("NO ESTA")
