# VARIABLES

# TiposDatos: cadenas de texto,caracteres, numeros enteros y flotantes (con coma), boleanos...

entero = 20
flotante = 2.0
caracter = 'a'
cadena = "holamundo"
booleano = True
variablelocal = 2

'''  
scope (alcance) de las variables

global (global): variable vive en todo el archivo (modulo) de codigo
local: variable vive sólo en el ámbito local, tenga en cuenta que si una variable local está creada,
        en el extremo izq de la identacion, esta será global.
constantes (import constant) : variable local que no se puede modificar

'''

# OPERANDS

# matematicos
a = 8 + 5  # = 13 (suma)
a = 8 * 5  # = 40 (mul)
a = 8 - 5  # = 3 (resta)
a = 8 / 5  # = 1,6 (division)
a = 8 // 5  # = 1 (division entera)
a = 8 % 5  # = 3 (modulo o resto) (util para saber la paridad)

# logicos
condicion = True and False
condicion2 = True or False
# unarios
negado = not True


# CONDITIONALS


if entero > 2 and entero < 10:
    print("entero > 2")
elif entero > 10:
    print("entero > 10")
else:
    print("resto")

# LOOPS

for i in range(0, 10, 2):
    print("hola")

while entero > 2:
    print("while")


# FUNCTIONS


def suma(a, b):
    return a+b


dato = suma(2, 5)
print(dato)

# TIP: la funcion len() devuelve el tamaño de cadenas, listas, diccionarios, etc


def cadena(input):
    return len(input)


tamano = cadena("carlos")
print(tamano)
