# Ej1 -  Write a function that receives a word, and prints the word repeated 10 times.
# def ejercicio1(word):

#     for i in range(10):     # en JS : for(var i = 1; i <=10; i+=2)
#         print(word, i)


# ejercicio1("Santi")


# Ej2 - Write a function that receives an persons age (number), and prints all the years lived by the person.

# def ejercicio2(edad):
#     a_nacimiento = 2020 - edad
#     for ano in range(a_nacimiento, 2021):
#         print(ano)


# ejercicio2(56)


# Ej 3 - Write a program that prints the odd numbers between 0 and 15.
# Use a function to find out when a single number is odd.

# def isOdd(n):
#     return n % 2 == 1


# def ejercicio3():
#     for n in range(0, 16):
#         if isOdd(n):
#             print(n)


# ejercicio3()


# Ej 4 - Escriba un programa que dado un número cualquiera,
# imprima un triángulo rectángulo de asteriscos,
# utilizando el número dado como el número filas de asteriscos.

# Ejemplo 1

# entrada: N = 2
# salida:
# *
# **

# Ejemplo 2

# entrada: N = 5
# salida:
# *
# **
# ***
# ****
# *****

# def ejercicio4(rows):
#     for n in range(1, rows+1):
#         print("*"*n)


# ejercicio4(2)


# Ej 5 - Escriba un programa para almacenar asignaturas de la clase (Mates, Fisica, Quimica y Lengua)
#        en una lista y las muestre cada uno de los nombres de las asignaturas en pantalla.


# def ejercicio5():
#     listaAsignaturas = ["Mates", "Fisica", "Quimica", "Lengua"]
#     # for val in listaAsignaturas:
#     #     print(val)
#     return listaAsignaturas


# asignaturas = ejercicio5()
# print("Ej5:", asignaturas)

# Ej 6 - Dada la lista del ejercicio anterior, haga una funcion que reciba la lista y un nueva asignatura, y devuelva
# la lista con la nueva asignatura. Ademas modifique el ejercicio5 para que devuelva la lista creada.


# def ejercicio6(lista, nueva):
#     lista.append(nueva)


# ejercicio6(asignaturas, "Historia")
# print("Ej6:", asignaturas)
# Ej 7 - Repita la misma accion del ejercicio6 pero sin modificar la lista original, o sea, cree una nueva lista de asignaturas
# añadiendole la asignatura Biologia.


# def ejercicio7(a):
#     lista = a.copy()
#     lista.append("Biologia")
#     return lista


# nueva = ejercicio7(asignaturas)
# print("Ej7:", asignaturas, nueva)

# Ej 8 - Dado una lista de objetos (llave,valor) usada para guardar datos de personas (dni, nombre), haga una funcion que
# añada una nueva persona, solo si no existe anteriormente.

# personas = {"1312411S": "Jose", "7352477N": "Laura", "1908754I": "Mario"}


# def añadirPersona(persona):
#     dni = persona[0]
#     nombre = persona[1]
#     if dni not in personas:
#         personas[dni] = nombre
#     else:
#         print("ERROR: Ya esta")
#     print(personas)


# nuevaPersona = ("8312411S", "Sandro")
# añadirPersona(nuevaPersona)

# Ej9 Dadas estas dos listas:
keys = ["Ten", "Twenty", "Thirty"]
values = [10, 20, 30]

# Haga una funcion que devuelva un diccionario asi:

# {"Ten":10,"Twenty":20,"Thirty":30}

# for v in lista:
#    xxxxxx
# for i in range(len(lista)):
#   xxxxxx


# def crearDiccionario(keys, values):
#     diccionario = dict()
#     for i in range(len(keys)):    # 0,1,2,...N-1
#         key = keys[i]
#         value = values[i]
#         diccionario[key] = value

#     print(diccionario)


# crearDiccionario(keys, values)


# Ej10 - Dada una lista de numeros del 1 al 10:

numeros = [1, 2, 3, 5, 5, 6, 2, 7, 9, 1]

# haga una funcion que encuentre un numero cualquiera (permita al usuario elegirlo),
#  y que una vez que lo encuentre detenga la busqueda y además imprima todos los números
# que están delante del encontrado junto con este (cada uno una sola vez).


def busqueda(numero):
    visitados = set()

    for e in numeros:
        visitados.add(e)
        if e == numero:
            return visitados

    return visitados


vis = busqueda(7)
print(vis)
