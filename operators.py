
def _not(a: bool) -> bool:
    return not a


def _and(a: bool) -> bool:
    return a and a


def _or(a: bool) -> bool:
    return a or a


print(_not(True))
print(_and(True))
print(_or(True))
