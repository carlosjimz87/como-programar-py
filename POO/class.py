class Rectangulo:
    # nombre = ""
    # ladoA: int = 0                            # prop 1
    # ladoB: int = 0                           # prop 2

    def __init__(self, n, lA, lB):         # constructor
        self.ladoA = lA
        self.ladoB = lB
        self.nombre = n

    def __str__(self) -> str:
        return f"Hola soy un rectangulo, me llamo {self.nombre}. Tengo un lado de {self.ladoA}mm y otro de {self.ladoB}mm"

    def area(self):                         # metodo
        area = self.ladoA * self.ladoB
        print(f'Tengo un area de {area} mm.')


# creamos una instancia (objecto) de la clase Rectangulo
rect = Rectangulo("Recty", 2, 4)
# rect.ladoA = 2
# rect.ladoB = 4

print(rect)

# usamos un metodo de la clase
rect.area()

# usar y modificar las  propiedades de la clase
rect.nombre = "Nuevy"
rect.ladoA = 10

print(rect)
