# crear una clase Persona, que tenga un nombre, un dni y un metodo saludar() donde describa como es.

class Hijo:

    def __init__(self, nombre):
        self.nombre = nombre


class Persona:

    def __init__(self, nombre, dni, hijo):
        self.nombre = nombre
        self.dni = dni
        self.hijo = hijo

    def saludar(self):
        print(
            f"obj: Persona con nombre {self.nombre} y dni {self.dni}. Tiene un hijo de nombre {self.hijo.nombre}")


persona1 = Persona("Santi", "32423434S", Hijo("Jose"))
persona2 = Persona("Carlos", "65652344N", Hijo("Pepe"))


persona1.saludar()
persona2.saludar()
